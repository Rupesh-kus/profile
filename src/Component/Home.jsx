import React, { Component } from "react";
import Image from "./Image.jpg";

export default class Home extends Component {
  render() {
    return (
      <div>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-md-6'>
              <img className='imageStyle' src={Image} alt='img no found' />
              <div className='col-md-12'>
                <h2> Profiles Links </h2>
                <div className='container1'>
                  <p>
                    <i class='fab fa-facebook-square'></i>
                    <a href='https://www.facebook.com/jay.prajapati.12139/'>
                      facebook
                    </a>
                  </p>
                  <p>
                    <i class='fab fa-gitlab'></i>
                    <a href='https://gitlab.com/Rupesh-kus'> gitlab</a>
                  </p>
                  <p>
                    <i class='fab fa-github-square'></i>
                    <a href='https://github.com/Rupesh-kus'> github</a>
                  </p>
                </div>
              </div>
            </div>
            <div className='col-md-6'>
              <h3>Hello! My name is Rupesh </h3>
              <p>
                I belongs to Bhaktapur-9, Kamalbinayak. I had completed by
                bachelor's degree in management form Khowpa College( Affilaited
                to Tribhiwan University).
              </p>
              <div class='list-group'>
                <a class='list-group-item list-group-item-action active'>
                  Personal Details
                </a>
                <a href='#' class='list-group-item list-group-item-dark'>
                  Name : Rupesh Kusi
                </a>
                <a href='#' class='list-group-item list-group-item-action'>
                  Address : Suryamadhi-1, Bhaktapur
                </a>
                <a href='#' class='list-group-item list-group-item-dark'>
                  Education : Bachelor completed
                </a>
                <a href='#' class='list-group-item list-group-item-action'>
                  contact.no : 9887139315
                </a>
                <a href='#' class='list-group-item list-group-item-dark'>
                  Email : serious201278@gmail.com
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
