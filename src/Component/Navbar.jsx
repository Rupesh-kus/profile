import React, { Component } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class Navbars extends Component {
  render() {
    return (
      <div className='HeadNav'>
        <Navbar className='Navb' fixed='top'>
          <Navbar.Brand href='#home'>My Portfolio</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='linkSty '>
              <Link className='navStyle' to='/'>
                Home
              </Link>
              <Link className='navStyle2' to='/About'>
                About
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
