import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./Component/Home";
import About from "./Component/About";
import Navbar from "./Component/Navbar";
import Footer from "./Component/Footer";
import "./App.css";
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Navbar />
        <Route path='/' exact component={Home} />
        <Route path='/About' component={About} />
        <Footer />
      </BrowserRouter>
    );
  }
}
export default App;
